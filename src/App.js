import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import Header from "./components/Header/Header";
import MyForm from "./components/MyForm/MyForm";
import DisplayUser from "./components/DisplayUser/DisplayUser";


function App() {

    const [user, setUser] = React.useState({user: null});

    return (
        <div className="container-fluid main">
            <Header/>
        <MyForm handleUser={setUser}/>
        <DisplayUser user={user}/>
        </div>
)
    ;
}

export default App;
